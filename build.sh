#!/usr/bin/env bash

VERBOSE=1
PACKAGE='my-ip'

# Filled in inside READ_CONFIG
INSTALL_ROOT=
PACKAGE_ROOT=

repeat() {
  for (( i = 0; i < $1; ++i )); do
    echo -n "$2"
  done
}

step() {
  local length
  local eq_cnt
  local eq_rep

  length=${#1}
  eq_cnt=$(( ( 70 - "$length" - 2 ) / 2 ))
  eq_rep=$(repeat "$eq_cnt" '=')

  # Echo
  echo -n "$eq_rep $1 $eq_rep"

  if [ "$(( "$eq_cnt" * 2 + "$length" + 2 ))" = "70" ]; then
    # For a even number only the linebreak is required.
    echo
  else
    # If we have an odd number, we need a trailing equal sign as well.
    echo '='
  fi
}

run() {
  local output
  local status

  if [ "$VERBOSE" -ge 1 ]; then
    echo "> $*"
  fi

  if [ "$VERBOSE" -ge 2 ]; then
    "$@"
  else
    output="$("$@" 2>&1)"
  fi

  status="$?"
  if [ "$status" != 0 ]; then
    case "$VERBOSE" in
      0)
        echo "> $*"
        ;&

      1)
        echo "$output"
        ;&

      *)
        step "FAILED WITH STATUS CODE $status"
        exit 1
        ;;
    esac
  fi
}

read_config() {
  if [ -n "$INSTALL_ROOT" ]; then
    return
  fi

  step 'READ CONFIG'

  local line
  local key
  local value
  local all_lines

  mapfile -t all_lines < <(stack path)
  for line in "${all_lines[@]}"; do
    key="${line%%:*}"
    value="${line#*: }"
    case "$key" in
      project-root )
        PACKAGE_ROOT="$value"
        ;;

      local-install-root )
        INSTALL_ROOT="$value"
        ;;

      * )
        ;;
    esac
  done

  if [ -z "$PACKAGE_ROOT" ] || [ -z "$INSTALL_ROOT" ]; then
    step FAILED
    exit 2
  fi

  run cd "$PACKAGE_ROOT"
}

clean_f() {
  step CLEAN
  run stack clean "$PACKAGE"
}

build_f() {
  step BUILD
  run stack build "$PACKAGE"
}

copy_f() {
  read_config
  step COPY
  run mkdir -p bin
  run cp -f "$INSTALL_ROOT/bin/$PACKAGE" bin
}

pack_f() {
  read_config
  step PACK
  run tar czf "$PACKAGE.keter" config bin
}

install_f() {
  read_config
  step INSTALL
  run cp -f "$PACKAGE.keter" /opt/keter/incoming
}

tasks_f() {
  step 'PRINT AVAILABLE TASKS'
  print_tasks
}

dev_f() {
  step 'DEVELOPMENT BUILD'
  run stack build --flag "$PACKAGE:dev" "$PACKAGE"
}

run_f() {
  step RUN
  exec stack exec my-ip
}

help_f() {
  step 'PRINT HELP'
  print_help
}

print_tasks() {
  local key
  for key in "${!ALL_STEPS[@]}"; do
    printf '%10s -> %s\n' "$key" "${ALL_STEPS[$key]}"
  done
}

print_help() {
  cat <<EOF
usage: build.sh [ -h | --help ] [ -v ] [task]
       build.sh [ -v ] start_task end_task

Options:
  -h, --help        display this help and exit.
  -t, --tasks       print a list of all available tasks and exit.
  -v, --verbose     increase verbosity
  -q, --quiet       decrease verbosity

For a list of all tasks see \`build.sh tasks\`.
EOF
}

print_invalid_task() {
  cat <<EOF
build.sh: invalid task name "$1".
For a list of all available tasks see \`build.sh --tasks\`.
EOF
  exit 3
}

print_invalid_task_count() {
  cat <<EOF
build.sh: invalid number of tasks given (saw "$1").
For a list of possible invocations see \`build.sh --help\`.
EOF
  exit 4
}

print_invalid_option() {
  cat <<EOF
build.sh: invalid option "$1".
For a list of all available options see \`build.sh --help\`.
EOF
  exit 5
}

declare -A ALL_STEPS=( [clean]=build [build]=copy [copy]=pack [pack]=install \
                       [install]='' [tasks]='' [help]='' [dev]=run [run]='' )

declare -a ARGS

for arg in "$@"; do
  case "$arg" in
    -h | --help )
      print_help
      exit
      ;;

    -v | --verbose )
      (( ++VERBOSE ))
      ;;

    -q | --quiet )
      (( --VERBOSE ))
      ;;

    -t | --tasks )
      print_tasks
      exit
      ;;

    -* )
      print_invalid_option "$arg"
      ;;

    * )
      ARGS+=( "$arg" )
      ;;
  esac
done


case "${#ARGS[@]}" in
  0 )
    build_f
    step DONE
    ;;

  1 )
    if [ -z "${ALL_STEPS[${ARGS[0]}]+X}" ]; then
      print_invalid_task "${ARGS[0]}"
    else
      "${ARGS[0]}_f"
      step DONE
    fi
    ;;

  2 )
    if [ -z "${ALL_STEPS[${ARGS[0]}]+X}" ]; then
      print_invalid_task "${ARGS[0]}"
    else
      next="${ARGS[0]}"
      while [ -n "$next" ]; do
        "${next}_f"
        if [ "$next" = "${ARGS[1]}" ]; then
          break
        fi

        next="${ALL_STEPS[$next]}"
      done
      step DONE
    fi
    ;;

  * )
    print_invalid_task_count "${#ARGS[@]}"
    ;;
esac
