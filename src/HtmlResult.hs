module HtmlResult (htmlResult) where

import Data.ByteString.Builder
import Data.Functor.Identity    (Identity(..))
import Data.Text.Lazy           (toStrict)

import Clay
import Lucid
import Lucid.Base

htmlResult :: Maybe Builder -> Builder
htmlResult =
  runIdentity . execHtmlT . combine . maybe nothingResult ipAddrResult

ipAddrResult :: Builder -> (Css, Html ())
ipAddrResult ip = (mempty, html')
  where html' = do h1_ "Deine IP-Adresse"
                   pre_ $ HtmlT (Identity (const ip, ()))

nothingResult :: (Css, Html ())
nothingResult = (css, html')
  where css = h1 ? color red
        html' = h1_ "IP-Adresse nicht feststellbar"

basicStyling :: Css
basicStyling = body ? do fontFamily [] [sansSerif]
                         textAlign center

combine :: (Css, Html ()) -> Html ()
combine (css, html') = doctypehtml_ $ do
  head_ $ do title_ "Deine IP-Adresse"
             style_ $ toStrict $
               renderWith compact [] $ basicStyling `mappend` css
  body_ html'
