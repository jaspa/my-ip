module Main (main) where

import           Control.Applicative                  ((<|>))
import qualified Data.ByteString                      as B
import           Data.ByteString.Builder
import           Data.Default
import           Data.List                            (find)
import           Data.Maybe                           (fromMaybe, mapMaybe)
import           Data.Monoid                          ((<>), mempty)
import           Data.Word8
import           Network.HTTP.Types
import           Network.Socket
import           Network.Wai
import           Network.Wai.Handler.Warp             (runEnv)
import           Network.Wai.Middleware.Approot       (envFallback)
import           Network.Wai.Middleware.Autohead
import           Network.Wai.Middleware.RequestLogger
import           Network.Wai.Parse                    (parseHttpAccept)
import           System.Log.FastLogger                (defaultBufSize,
                                                       newStdoutLoggerSet)

import HtmlResult

main :: IO ()
main = do
  logSet    <- newStdoutLoggerSet defaultBufSize
  approot   <- envFallback
  reqLogger <- mkRequestLogger def{ outputFormat = Apache FromFallback
                                  , destination = Logger logSet
                                  }
  runEnv 3333 $ approot $ reqLogger $ autohead $ validateRequest app

validateRequest :: Middleware
validateRequest app' req send'
  | requestMethod req /= "GET" =
    send' $ errorResponse methodNotAllowed405 "METHOD NOT ALLOWED"
  | pathInfo req /= [] =
    send' $ errorResponse notFound404 "NOT FOUND"
  | otherwise = app' req send'

app :: Application
app request send' = send' $
  let accepted = parseHttpAccept $ fromMaybe "" $
                   lookup hAccept $ requestHeaders request

      headerAddr = find (\(h,_) -> h `elem` ["x-real-ip", "x-forwarded-for"]) $
                        requestHeaders request

      reqAddr = case remoteHost request of
                  SockAddrInet _ addr      -> Just $ formatAddr addr
                  SockAddrInet6 _ _ addr _ -> Just $ formatAddr6 addr
                  _                        -> Nothing

      maddr = byteString . snd <$> headerAddr <|> reqAddr

      selectRep "*/*"        = html
      selectRep "text/*"     = html
      selectRep "text/html"  = html
      selectRep "text/plain" = plain
      selectRep _            = Nothing

      html  = Just ("text/html; charset=utf-8", htmlResult maddr)
      plain = Just ("text/plain; charset=utf-8", fromMaybe mempty maddr)

   in case mapMaybe (selectRep . B.takeWhile (/= _semicolon)) accepted of
        (ct, b):_ -> responseBuilder ok200 [(hContentType, ct)] b
        []        -> errorResponse notAcceptable406 "NOT ACCEPTABLE"

formatAddr :: HostAddress -> Builder
formatAddr (hostAddressToTuple -> (a, b, c, d)) =
  concatB _period word8Dec [a,b,c,d]

formatAddr6 :: HostAddress6 -> Builder
formatAddr6 (hostAddress6ToTuple -> (a, b, c, d, e, f, g, h)) =
  concatB _colon word16HexFixed [a,b,c,d,e,f,g,h]

concatB :: Word8 -> (a -> Builder) -> [a] -> Builder
concatB (word8 -> p) f (a:as) = f a <> mconcat [p <> f a' | a' <- as]
concatB _ _ _ = ""

errorResponse :: Status -> Builder -> Response
errorResponse s = responseBuilder s [(hContentType, "text/plain")]
